import React from 'react'
import { Layout, Menu, Breadcrumb } from 'antd';
import './headerS.css'
const Header = () => {
    const isLogin = localStorage.getItem('isLogin')

    return (
        <div>
            <Menu
                theme="dark"
                mode="horizontal"
                defaultSelectedKeys={['2']}
                style={{ lineHeight: '64px' }}
            >
                <Menu.Item ><a href="/">Home</a></Menu.Item>
                <Menu.Item ><a href="/about">About</a></Menu.Item>
                <Menu.Item ><a href="/skill">Skill</a></Menu.Item>
                <Menu.Item > <a href="/gpa">Gpa</a></Menu.Item>
                <Menu.Item > <a href="/anime">Anime</a></Menu.Item>
                {
                    isLogin == 'true' &&
                    <Menu.Item > <a href="/processLogout">Logout</a></Menu.Item>
                }
            </Menu>
            <div className="logoName">
                <h1>RAVIPON SUWANPRADHES</h1>
                <h3>602110152</h3>
            </div>
        </div>


    )

}
export default Header