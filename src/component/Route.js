import React, {Component} from 'react';
import "bootstrap/dist/css/bootstrap.css"
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import AboutPage from '../cvPage/AboutPage';
import SkillPage from '../cvPage/SkillPage';
import GpaPage from '../cvPage/GpaPage';
import Header from './Header';
import LoginPage from '../cvPage/LoginPage';
import AnimePage from '../cvPage/AnimePage';
// import LoginPage from '../cvPage/UserPage2';
import PrivateRoute from './PrivateRoute';
import WelcomePage from '../cvPage/WelcomePage';

const MainRouting =()=>{
    return(
    <BrowserRouter>       
    {/* บันทึกค่าlogin */}
    <Route path="/processLogin" render={() =>{
        alert("login here")
        localStorage.setItem("isLogin",true);
        return <Redirect to="/gpa"></Redirect>
    }} /> 
     <Route path="/processLogout" render={() =>{
        alert("logout here")
        localStorage.setItem("isLogin",false);
        return <Redirect to="/about"></Redirect>
    }} /> 

        <Route path="/" component={WelcomePage}  exact='true'/> 
        <Route path="/about" component={AboutPage} exact='true'/>
        <Route path="/skill" component={SkillPage} exact='true'/>
        <PrivateRoute path="/gpa" component={GpaPage} />
        <Route path="/login" component={LoginPage} exact='true'/>
        <Route path="/anime" component={AnimePage} exact='true'/>
        {/* <Route path="/user" component={UserPage} /> */}

    </BrowserRouter>
    )}

    export default MainRouting
