import React from 'react'
import "./AboutPage.css";
import { Menu, Carousel,Layout } from 'antd';
import FooterTab from './FooterTab';
import pre from '../152/pre.jpg';
import ning from '../152/ning.jpg';

const {  Content } = Layout;
const WelcomePage = () => {
    const isLogin = localStorage.getItem('isLogin')

    return (
        <div className='bggPage'>
            <Menu
                theme="dark"
                mode="horizontal"
                defaultSelectedKeys={['2']}
                style={{ lineHeight: '64px' }}
            >
                <Menu.Item ><a href="/about">About</a></Menu.Item>
                <Menu.Item ><a href="/skill">Skill</a></Menu.Item>
                <Menu.Item > <a href="/gpa">Gpa</a></Menu.Item>
                <Menu.Item > <a href="/anime">Anime</a></Menu.Item>
                {
                    isLogin == 'true' &&
                    <Menu.Item > <a href="/processLogout">Logout</a></Menu.Item>
                }
            </Menu>
            <div className="logoName">
                <h1>RAVIPON SUWANPRADHES</h1>
                <h3>602110152</h3>
            </div>
            <Layout>
            <Content  className='bggPage'>
            <div className='container '>
                <Carousel autoplay >
                    <div>
                        <img src={pre} className='imgSlide'/>
                    </div>
                    <div>
                    <img src={ning} className='imgSlide'/>
                    </div>
                </Carousel>
            </div>
            </Content>
            </Layout>
            <FooterTab />
        </div>
    )

}
export default WelcomePage