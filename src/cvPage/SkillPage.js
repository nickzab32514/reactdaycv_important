import React from 'react'
import light from '../152/light.png';
import html from '../152/html.png';
import css from '../152/css.png';
import micro from '../152/micro.png';
import pre1 from '../152/man.jpg';
import pre2 from '../152/net.jpg';
import pre3 from '../152/viu.jpg';
import pre4 from '../152/sao.jpg';
import "./AboutPage.css";
import Header from '../component/Header';
import FooterTab from './FooterTab';

class SkillPage extends React.Component {
    render() {

        return (
            <div className='bgAbout'>
                <Header />
                <div class="container " id="bgpink">
                    <div>
                        <div className='colorBg'>
                            <h1 class="masthead-subheading font-weight-light mb-0 bottomline">Skill</h1>
                            <div class="card-deck cTop margincard">
                                <div class="card ">
                                    <img class="card-img-top sizeskill" src={css} alt="Card image cap" />
                                    <div class="card-body fontcenter">
                                        <h5 class="card-title">CSS</h5>
                                    </div>
                                </div>
                                <div class="card">
                                    <img class="card-img-top sizeskill" src={html} alt="Card image cap" />
                                    <div class="card-body fontcenter">
                                        <h5 class="card-title">HTML</h5>
                                    </div>
                                </div>
                                <div class="card">
                                    <img class="card-img-top sizeskill" src={light}
                                        alt="Card image cap" />
                                    <div class="card-body fontcenter">
                                        <h5 class="card-title">Adobe lightroom</h5>
                                    </div>
                                </div>
                                <div class="card">
                                    <img class="card-img-top sizeskill" src={micro}
                                        alt="Card image cap" />
                                    <div class="card-body fontcenter">
                                        <h5 class="card-title fontcenter">Microsoft office</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='colorBg '>
                            <h1 class="masthead-subheading font-weight-light mb-0 bottomline">Preferrence</h1>

                            <div class="card-deck cTop margincard">
                                <div class="card ">
                                    <img class="card-img-top sizeskillPre" src={pre1} alt="Card image cap" />
                                    <div class="card-body fontcenter">
                                        <h5 class="card-title">Football</h5>
                                    </div>
                                </div>
                                <div class="card">
                                    <img class="card-img-top sizeskillPre" src={pre2} alt="Card image cap" />
                                    <div class="card-body fontcenter">
                                        <h5 class="card-title">Netflix</h5>
                                    </div>
                                </div>
                                <div class="card">
                                    <img class="card-img-top sizeskillPre" src={pre3}
                                        alt="Card image cap" />
                                    <div class="card-body fontcenter">
                                        <h5 class="card-title">Viu</h5>
                                    </div>
                                </div>
                                <div class="card">
                                    <img class="card-img-top sizeskillPre" src={pre4}
                                        alt="Card image cap" />
                                    <div class="card-body fontcenter">
                                        <h5 class="card-title fontcenter">Read manga</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <FooterTab />
            </div>
        );
    }
}

export default SkillPage