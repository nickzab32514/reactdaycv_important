import React from 'react'
import "./AboutPage.css";
import nick from '../152/nick.jpg';
import Header from '../component/Header';
import FooterTab from './FooterTab';
class AboutPage extends React.Component {


    render() {
        return (
            <div className='bgAbout'>
                <Header/>
                <div class="container shadow">
                    <div class="row" id="about">
                        <div class="col-1 bGgray">
                        </div>
                        <div class="col-5">
                            <img src={nick} class="sizeProfile" />
                        </div>
                        <div class="col-5">
                            <div class="row justify-content-center">
                                <h1 id="abouts">----About us---</h1>
                            </div>
                            <div class="row justify-content-start">
                                <div class="col-10 " id="info">
                                    <h1 class="info">Information</h1>
                                    <p>Name:Ravipon Suwanpradhes</p>
                                    <p>Nickname:Nicky</p>
                                    <p>Age:22
                        </p>
                                    <p>DOB:10 Oct 1998</p>
                                </div>
                            </div>
                            <div class="row justify-content-start">
                                <div class="col-10" id="info">
                                    <h1 class="info">Contract</h1>
                                    <p>Phone:0869138883</p>
                                    <p>Email:nickzab32514@gmail.com>
                        </p>
                                    <p>Line:nkz_nickos</p>
                                    <p>Adress:-</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-1 bGgray">
                        </div>
                    </div>
                </div>
                <FooterTab></FooterTab>
            </div>
        );
    }
}

export default AboutPage