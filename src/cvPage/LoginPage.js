import React from 'react'
import './login.css'
import { Layout } from 'antd';
const { Content } = Layout;
class LoginPage extends React.Component {
    render() {

        return (
            <Layout>
                <Content className='pink'>
                    <div style={{backgroundColor:'gray'}}>
                        <div class="container">
                            <div class='card'>
                                <div ><h1 className='midFont'>Login page</h1></div>
                                <form className='changForm'>
                                    <div class="row inputSt">
                                        <label class="col-sm-3" for="exampleInputEmail1">Email</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control sizeinput" id="exampleInputEmail1" aria-describedby="emailHelp" />
                                        </div>
                                    </div>
                                    <div class="row inputSt">
                                        <label class="col-sm-3" for="exampleInputPassword1">Password</label>
                                        <div class="col-sm-8">
                                            <input type="password" class="form-control sizeinput" id="exampleInputPassword1" />
                                        </div>
                                    </div>
                                    <input type="button" value="Login" className='buttomLg' onClick={
                                        () => {
                                            //Redirect
                                            window.location = "/processLogin"
                                        }
                                    }
                                    />
                                </form>
                            </div>
                        </div>
                    </div>
                </Content>
            </Layout>
        );
    }
}

export default LoginPage