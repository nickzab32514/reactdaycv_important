import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import "bootstrap/dist/css/bootstrap.css"
import * as serviceWorker from './serviceWorker';
import MainRouting from './component/Route';
import 'antd/dist/antd.css';
// import { BrowserRouter, Route, Redirect } from 'react-router-dom';
// import AboutPage from './cvPage/AboutPage';
// import SkillPage from './cvPage/SkillPage';
// import GpaPage from './cvPage/GpaPage';
// import Header from './component/Header';
// import LoginPage from './cvPage/LoginPage';


//how to use localstorage 

//setter การบันทึกลงใน local storage
// localStorage.setItem("a","Helloworld");

//getter
// const a=localStorage.getItem("a");
// alert(a);

// const PrivateRoute=({path,component:Component})=>(
//     <Route path={path} render={(props) =>{
//         const isLogin=localStorage.getItem('isLogin')
//         if(isLogin=='true')
//         return <Component {...props}/>
//         else
//         return <Redirect to="/login"></Redirect>

//     }} /> 

// )

// const MainRouting =()=>{
//     return(
//     <BrowserRouter>       
//     {/* บันทึกค่าlogin */}
//     <Route path="/processLogin" render={() =>{
//         alert("login here")
//         localStorage.setItem("isLogin",true);
//         return <Redirect to="/gpa"></Redirect>
//     }} /> 
//      <Route path="/processLogout" render={() =>{
//         alert("logout here")
//         localStorage.setItem("isLogin",false);
//         return <Redirect to="/about"></Redirect>
//     }} /> 

//         <Route path="/" component={Header} /> 
//         <Route path="/about" component={AboutPage} />
//         <Route path="/skill" component={SkillPage} />
//         <PrivateRoute path="/gpa" component={GpaPage} />
//         <Route path="/login" component={LoginPage} />
//     </BrowserRouter>
//     )}
ReactDOM.render(<MainRouting/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
